# ============================================================
# Zoom Label extension for Falkon
# Copyright (C) 2019 Zdravko Mitov <mitovz@mail.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ============================================================
import Falkon
from PySide2 import QtCore
from zoomlabel import zoom_label


class ZoomLabelExtension(Falkon.PluginInterface, QtCore.QObject):
    lwidgets = {}

    def init(self, state, settingsPath):

        plugins = Falkon.MainApplication.instance().plugins()

        plugins.mainWindowCreated.connect(self.mainWindowCreated)
        plugins.mainWindowDeleted.connect(self.mainWindowDeleted)

        if state == Falkon.PluginInterface.LateInitState:
            for window in Falkon.MainApplication.instance().windows():
                self.mainWindowCreated(window)

    def unload(self):
        for window in Falkon.MainApplication.instance().windows():
            self.mainWindowDeleted(window)

    def testPlugin(self):
        return True

    def mainWindowCreated(self, window):
        lwidget = zoom_label.ZoomLabel(window)
        window.navigationBar().addWidget(lwidget, lwidget.id(), lwidget.name())
        self.lwidgets[window] = lwidget

    def mainWindowDeleted(self, window):
        if window not in self.lwidgets:
            return
        lwidget = self.lwidgets[window]
        window.navigationBar().removeWidget(lwidget.id())
        del self.lwidgets[window]


Falkon.registerPlugin(ZoomLabelExtension())
