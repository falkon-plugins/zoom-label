# ============================================================
# Zoom Label extension for Falkon
# Copyright (C) 2019 Zdravko Mitov <mitovz@mail.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ============================================================
from PySide2 import QtWidgets


class ZoomLabel(QtWidgets.QLabel):

    def __init__(self, window):
        super().__init__()
        self.win = window
        self.setToolTip("Will be active after zoom level or page are changed!")
        self.setFrameStyle(QtWidgets.QFrame.StyledPanel)
        self.win.tabWidget().changed.connect(self.on_tab_changed)
        self.on_tab_changed()

    def id(self):
        return "zoom-label"

    def name(self):
        return "Zoom label"

    def on_which_view(self):
        return self.win.weView()

    def on_value_changed(self, value):
        lbl = None
        view = self.on_which_view()
        if view:
            lbl = int(view.zoomFactor() * 100)
        if value == 6:
            self.hide()
            self.setVisible(False)
        else:
            self.setVisible(True)
            self.setNum(lbl)
            self.setToolTip("Current zoom level %d %%" % lbl)
            self.show()

    def on_tab_changed(self):
        view = self.on_which_view()
        if view:
            self.on_value_changed(view.zoomLevel())
            view.zoomLevelChanged.connect(self.on_value_changed)
